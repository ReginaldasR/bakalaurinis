#!/usr/bin/python
import os
current_dir = os.getcwd()

activate_this = current_dir + '/flask/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))
import subprocess, sys
if sys.platform == 'win32':
    bin = 'Scripts'
else:
    bin = 'bin'
subprocess.call([os.path.join('flask', bin, 'pip'), 'install', 'flask<0.10'])
subprocess.call([os.path.join('flask', bin, 'pip'), 'install', 'flask-sqlalchemy'])
subprocess.call([os.path.join('flask', bin, 'pip'), 'install', 'sqlalchemy<=0.7.10'])
subprocess.call([os.path.join('flask', bin, 'pip'), 'install', 'sqlalchemy-migrate'])
subprocess.call([os.path.join('flask', bin, 'pip'), 'install', 'WTForms'])
subprocess.call([os.path.join('flask', bin, 'pip'), 'install', 'Flask-WTF'])
subprocess.call([os.path.join('flask', bin, 'pip'), 'install', 'flask-mail'])
subprocess.call([os.path.join('flask', bin, 'pip'), 'install', 'simplejson'])
subprocess.call([os.path.join('flask', bin, 'pip'), 'install', 'jsonpickle'])
subprocess.call([os.path.join('flask', bin, 'pip'), 'install', 'xlwt'])
subprocess.call([os.path.join('flask', bin, 'pip'), 'install', 'xlrd'])
subprocess.call([os.path.join('flask', bin, 'pip'), 'install', 'xhtml2pdf'])
subprocess.call([os.path.join('flask', bin, 'pip'), 'install', 'weasyprint'])
subprocess.call([os.path.join('flask', bin, 'pip'), 'install', 'flask-weasyprint'])