# -*- coding: utf-8 -*-
from ..extensions import db
from sqlalchemy import Column
ADMIN = 0
USER = 1


class Person(db.Model):
    id = Column(db.Integer, primary_key=True)
    first_name = Column(db.String(128), unique=False)
    second_name = Column(db.String(128), nullable=False, unique=False)
    email = Column(db.String(128), nullable=True, unique=True)
    gender = Column(db.Integer, nullable=True, unique=False)
    class_number = Column(db.Integer, nullable=True, unique=False)
    class_letter = Column(db.String(2), nullable=True, unique=False)
    phone = Column(db.String(11), nullable=True, unique=False)

    def __repr__(self):
        return 'Person < %s >' % self.id


class User(db.Model):
    id = Column(db.Integer, primary_key=True)
    username = Column(db.String(128), nullable=False, unique=True)
    password = Column(db.String(512), nullable=False)
    salt = Column(db.String(512), nullable=True)
    role = Column(db.Integer, nullable=False, unique=False)
    person = db.relationship('Person')
    person_id = Column(db.Integer, db.ForeignKey('person.id'), nullable=False)
    first_login = Column(db.Integer, unique=False, default=0)
    first_password = Column(db.String(12), unique=False, nullable=True)

    def __repr__(self):
        return 'User < %s >' % self.username