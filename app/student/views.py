# -*- coding: utf-8 -*-
from functools import wraps
import datetime
from app.auth import check_password
from app.auth.utils import hash_password
from app.emails import send_email
from app.extensions import db
from app.lessons.models import Lesson, Application, ChosenLesson, Group
from app.settings import Settings
import config
from flask.ext.weasyprint import render_pdf, HTML
import re
from sqlalchemy.sql.functions import user
from ..utils import create_pdf
from ..auth import login_required
from ..auth.autorization import role_required
from flask.views import MethodView
from ..users.models import USER, User, ADMIN
from flask import Blueprint, g, render_template, request, flash, redirect, url_for, session, Response

student = Blueprint('student', __name__, url_prefix='/student')


def check_step_real(step):
    def real_decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            application = get_application()
            if application is None:
                create_new_application()
                return redirect(url_for('student.first_step'))
            date_string = Settings.query.filter_by(name='date_until').first().value
            date_until = datetime.datetime.strptime(date_string, "%Y-%m-%d")
            if datetime.datetime.now() > date_until:
                application.step = 3
            if step == application.step:
                return f(*args, **kwargs)
            else:
                if application.step == 0:
                    return redirect(url_for('student.first_step'))
                elif application.step == 1:
                    return redirect(url_for('student.second_step'))
                elif application.step == 2:
                    return redirect(url_for('student.application'))

        return wrapper

    return real_decorator


class StudentIndexPage(MethodView):
    decorators = [role_required(USER), login_required]

    def get(self):
        return render_template('student/index.html', active='index')


class StudentProfilePage(MethodView):
    decorators = [role_required(USER), login_required]

    template = 'student/profile.html'

    def __init__(self):
        self.user = User.query.filter_by(id=g.user.id).first()

    def get(self):
        return render_template(self.template, user=self.user, active='profile')

    def post(self):
        form = request.form
        #validacija
        self.user.person.email = form['email']
        self.user.first_login = 0
        db.session.commit()
        flash(u'Sėkmingai atnaujinote asmeninę informaciją', 'success')
        return render_template(self.template, user=self.user)


class StudentFormFirstStep(MethodView):
    decorators = [check_step_real(0), role_required(USER), login_required]

    def get(self):
        self.check_for_application()
        first_lessons = Lesson.query.filter_by(group_id=1).all()
        lesson_types = set([lesson.lesson_type for lesson in first_lessons])
        lessons_to_choose = {}
        for lesson_type in lesson_types:
            if lesson_type is not None:
                lessons = Lesson.query.filter_by(group_id=1, lesson_type=lesson_type).all()
                lessons_to_choose[lesson_type.name] = lessons
            else:
                lessons = Lesson.query.filter_by(group_id=1, lesson_type=None).all()
                lessons_to_choose["required"] = lessons
        second_lessons = Lesson.query.filter_by(group_id=2).all()
        return render_template('student/first_step.html', lessons=lessons_to_choose, active='application',
                               second_lessons=second_lessons)

    def post(self):
        #for item in request.form:
        #    lesson = Lesson.query.filter_by(id=request.form[item]).first()
        #    db.session.add(ChosenLesson(lesson_id=request.form[item], lesson=lesson,
        #                                application=get_application(), application_id=get_application().id))
        #    application = get_application()
        #    application.step = 1
        #    db.session.commit()
        first_lessons = Lesson.query.filter_by(group_id=1).all()
        lessons_to_choose = {}
        lesson_types = set([lesson.lesson_type for lesson in first_lessons])
        for lesson_type in lesson_types:
            if lesson_type is not None:
                lessons = Lesson.query.filter_by(group_id=1, lesson_type=lesson_type).all()
                lessons_to_choose[lesson_type.name] = lessons
            else:
                lessons = Lesson.query.filter_by(group_id=1, lesson_type=None).all()
                lessons_to_choose["required"] = lessons
        need_to_choose = len(lessons_to_choose) + len(lessons_to_choose["required"])
        second_lessons = Lesson.query.filter_by(group_id=2).all()
        if need_to_choose != request.form:
            flash(u'Nepasirinktos privalomos pamokos', 'error')
            return render_template('student/first_step.html',
                                   lessons=lessons_to_choose, active='application', second_lessons=second_lessons,
                                   chosen=[int(request.form[k]) for k in request.form])
        print len(lessons_to_choose) + len(lessons_to_choose["required"])
        print len(request.form)
        return redirect(url_for('student.second_step'))

    def check_for_application(self):
        if Application.query.filter_by(user=g.user).first() is None:
            create_new_application()


def create_new_application():
    application = Application(user=g.user, user_id=g.user.id, step=0,seen=0)
    db.session.add(application)
    db.session.commit()


class StudentFormSecondStep(MethodView):
    decorators = [check_step_real(1), role_required(USER), login_required]

    def get(self):
        second_lesson = Lesson.query.filter_by(group_id=2).all()
        lesson_types = set([lesson.lesson_type for lesson in second_lesson])
        lessons_to_choose = {}
        for lesson_type in lesson_types:
            if lesson_type is not None:
                lessons = Lesson.query.filter_by(group_id=2, lesson_type=lesson_type).all()
                chosen_lessons = ChosenLesson.query.filter_by(application=get_application()).all()
                chosen_lessons_names = [lesson.lesson.name for lesson in chosen_lessons]
                lesson_filtered = [lesson for lesson in lessons if lesson.name not in chosen_lessons_names]
                lessons_to_choose[lesson_type.name] = lesson_filtered
        return render_template('student/first_step.html', lessons=lessons_to_choose, active='application')

    def post(self):
        for item in request.form:
            lesson = Lesson.query.filter_by(id=request.form[item]).first()
            db.session.add(ChosenLesson(lesson_id=request.form[item], lesson=lesson,
                                        application=get_application(), application_id=get_application().id))
            application = get_application()
            application.step = 2
            db.session.commit()
        return redirect(url_for('student.third_step'))


class StudentFormThirdStep(MethodView):
    decorators = [role_required(USER), login_required]

    def get(self):
        third_lessons = Lesson.query.filter_by(group_id=3).all()
        #for lesson_type in lesson_types:
        #    if lesson_type is not None:
        #        lessons = Lesson.query.filter_by(group_id=1, lesson_type=lesson_type).all()
        #        lessons_to_choose[lesson_type.name] = lessons
        #    else:
        #        lessons = Lesson.query.filter_by(group_id=1, lesson_type=None).all()
        #        lessons_to_choose["required"] = lessons
        return render_template('student/thirdStep.html', lessons=third_lessons, active='application')

    def post(selfself):
        max_lesson = Settings.query.filter_by(name='max_lessons').first().value
        min_lesson = Settings.query.filter_by(name='min_lessons').first().value
        lessons_from_checkbox = []
        lesson_ids = request.form.getlist('free_to_take')
        for lesson_id in lesson_ids:
            lessons_from_checkbox.append(Lesson.query.filter_by(id=lesson_id).first())
        chosen_lessons = ChosenLesson.query.filter_by(application=get_application()).all()
        first_lesson_count = 1
        second_lesson_count = 1
        for lesson in chosen_lessons:
            first_lesson_count += lesson.lesson.first_year
            second_lesson_count += lesson.lesson.second_year
        for lesson in lessons_from_checkbox:
            first_lesson_count += lesson.first_year
            second_lesson_count += lesson.second_year
        print first_lesson_count
        print second_lesson_count
        if (int(max_lesson) < first_lesson_count or int(max_lesson) < second_lesson_count):
            flash(u'Pasirinkta per daug pamoku leidziama ' + max_lesson, 'error')
            redirect(url_for('student.third_step'))
        elif (int(min_lesson) > first_lesson_count or int(min_lesson) > second_lesson_count):
            flash(u'Pasirinkta per mazai pamoku minimaliai pasirinkti reikia ' + min_lesson, 'error')
            redirect(url_for('student.third_step'))
        for lesson in lessons_from_checkbox:
            db.session.add(ChosenLesson(lesson_id=lesson.id, lesson=lesson,
                                        application=get_application(), application_id=get_application().id))
            application = get_application()
            application.step = 3
            db.session.commit()
        return redirect(url_for('student.application'))


class StudentGeneratePdf(MethodView):
    decorators = [role_required(USER, ADMIN), login_required]

    def get(self):
        date_now = datetime.datetime.now()
        date = {'year': date_now.year}
        if len(str(date_now.month)) == 2:
            date['month'] = date_now.month
        else:
            date['month'] = '0' + str(date_now.month)
        if len(str(date_now.day)) == 2:
            date['day'] = date_now.day
        else:
            date['day'] = '0' + str(date_now.day)
        if 'user_id_for_application' in session:
            user = User.query.filter_by(id = session['user_id_for_application']).first()
        else:
            user = g.user
        first_year = int(date_now.year) + 1
        second_year = int(date_now.year) + 2
        chosen_lessons = ChosenLesson.query.filter_by(application=get_application()).all()
        lesson_to_template = {}
        lesson_groups = set([lesson.lesson.group for lesson in chosen_lessons])
        first_lesson_count = 1
        second_lesson_count = 1
        for group in lesson_groups:
            lesson_to_template[group.name] = [lesson.lesson for lesson in chosen_lessons if
                                              lesson.lesson.group == group]
        for lesson in chosen_lessons:
            first_lesson_count += lesson.lesson.first_year
            second_lesson_count += lesson.lesson.second_year
        max_lesson = Settings.query.filter_by(name='max_lessons').first()
        min_lesson = Settings.query.filter_by(name='min_lessons').first()

        html = render_template('student/pdf/test.html', user=user, date=date, first_year=first_year,
                               second_year=second_year, lessons=lesson_to_template, max=max_lesson, min=min_lesson,
                               second_lesson_count=second_lesson_count, first_lesson_count=first_lesson_count)
        return render_pdf(HTML(string=html))


class StudentApplicationForm(MethodView):
    decorators = [role_required(USER), login_required]

    def get(self):
        date_until = Settings.query.filter_by(name='date_until').first().value
        lesson_count_first = 0
        lesson_count_second = 0
        chosen_lessons = ChosenLesson.query.filter_by(application=get_application()).all()
        for lesson in chosen_lessons:
            lesson_count_first += lesson.lesson.first_year
            lesson_count_second += lesson.lesson.second_year
        return render_template('student/application.html', application=get_application(), active='application',
                               first=lesson_count_first, second=lesson_count_second)


class StudentDeleteApplication(MethodView):
    decorators = [role_required(USER), login_required]

    def get(self):
        application = get_application()
        chosen_lessons = ChosenLesson.query.filter_by(application=application).all()
        db.session.delete(application)
        for lesson in chosen_lessons:
            db.session.delete(lesson)
        db.session.commit()
        return redirect(url_for('student.index'))


def get_application():
    if g.user.role == ADMIN:
        id = session['user_id_for_application']
        user = User.query.filter_by(id=id).first()
        return Application.query.filter_by(user=user).first()
    return Application.query.filter_by(user=g.user).first()


class StudentLessonView(MethodView):
    decorators = [role_required(USER), login_required]

    def get(self):
        groups = Group.query.all()
        grouped_lessons = {}
        for group in groups:
            grouped_lessons[group.name] = Lesson.query.filter_by(group=group).all()
        return render_template('student/lessons.html', grouped_lessons=grouped_lessons, active='lessons')


class StudentContacts(MethodView):
    decorators = [role_required(USER), login_required]

    def post(self):
        user = g.user
        errors = 0
        for item in request.form:
            print type(request.form[item])
            if not request.form[item]:
                errors += 1
        if errors != 0:
            flash(u'Ivyko klaida atnaujinant duomenis', 'error')
            return redirect(url_for('student.profile'))
        user.person.email = request.form['email']
        user.person.phone = request.form['phone']
        db.session.add(user)
        db.session.commit()
        flash(u'Sėkmingai atnaujina informacija', 'success')
        return redirect(url_for('student.profile'))


class StudentChangePassword(MethodView):
    decorators = [role_required(USER), login_required]

    def post(self):
        user = g.user
        errors = 0
        for item in request.form:
            if not request.form[item]:
                errors += 1
        if errors != 0:
            flash(u'Ivyko klaida atnaujinant slaptažodį', 'error')
            return redirect(url_for('student.profile'))
        if check_password(request.form['old_password'], user.password, user.salt):
            if request.form['new_password'] == request.form['repeat_password']:
                user.salt, user.password = hash_password(request.form['new_password'])
                db.session.add(user)
                db.session.commit()
                flash(u'Sėkmingai pakeistas slaptažodis', 'success')
        return redirect((url_for('student.profile')))


class StudentFirstLogin(MethodView):
    decorators = [role_required(USER)]

    def get(self):
        return render_template('student/first_login.html', user=user)

    def post(self):
        user = g.user
        errors = 0
        for item in request.form:
            if not request.form[item]:
                errors += 1
        if errors == 0:
            if not re.match(r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$", request.form['email']):
                flash(u'Netinkamas el. pašto formatas', 'error')
                return redirect(url_for('student.first_login'))
            if not re.match(r"^^\+[0-9]+$", request.form['phone']):
                flash(u'Netinkamas telefono numerio formatas', 'error')
                return redirect(url_for('student.first_login'))
            user.person.email = request.form['email']
            user.person.phone = request.form['phone']
            user.salt, user.password = hash_password(request.form['password'])
            user.first_login = 1
            db.session.add(user)
            db.session.commit()
            text = "Sekmingai atnaujinote asmenine informacija <br />" \
                   "Jusu prisijungimo vardas yra: <b>%s</b><br /> \n" \
                   "Slaptazodis: <b>%s</b>" % (user.username, request.form['password'])
            send_email('Jūsų duomenys profiliavimo sistemoje',
                       config.ADMIN_EMAIL, [user.person.email, ], text, text)
            return redirect(url_for('student.index'))

        else:
            flash(u'Visi laukai yra privalomi', 'error')
            return render_template('student/first_login.html', user=user)


class StudentRequiredLessons(MethodView):
    decorators = [role_required(USER), login_required]

    def get(self):
        first_lessons = Lesson.query.filter_by(group_id=1).all()
        lesson_types = set([lesson.lesson_type for lesson in first_lessons])
        lessons_to_choose = {}
        for lesson_type in lesson_types:
            if lesson_type is not None:
                lessons = Lesson.query.filter_by(group_id=1, lesson_type=lesson_type).all()
                lessons_to_choose[lesson_type.name] = lessons
            else:
                lessons = Lesson.query.filter_by(group_id=1, lesson_type=None).all()
                lessons_to_choose["required"] = lessons
        second_lessons = Lesson.query.filter_by(group_id=2).all()
        application = get_application()
        if application is None:
            create_new_application()
            return render_template('student/first_step.html', lessons=lessons_to_choose, active='application',
                                   second_lessons=second_lessons)
        else:
            chosen = ChosenLesson.query.filter_by(application=application)
            return render_template('student/first_step.html',
                                   lessons=lessons_to_choose, active='application', second_lessons=second_lessons,
                                   chosen=[int(c.lesson.id) for c in chosen])


    def post(self):
        first_lessons = Lesson.query.filter_by(group_id=1).all()
        lessons_to_choose = {}
        lesson_types = set([lesson.lesson_type for lesson in first_lessons])
        for lesson_type in lesson_types:
            if lesson_type is not None:
                lessons = Lesson.query.filter_by(group_id=1, lesson_type=lesson_type).all()
                lessons_to_choose[lesson_type.name] = lessons
            else:
                lessons = Lesson.query.filter_by(group_id=1, lesson_type=None).all()
                lessons_to_choose["required"] = lessons
        need_to_choose = len(lessons_to_choose) + len(lessons_to_choose["required"])
        second_lessons = Lesson.query.filter_by(group_id=2).all()
        if need_to_choose != len(request.form):
            flash(u'Nepasirinktos privalomos pamokos', 'error')
            return render_template('student/first_step.html',
                                   lessons=lessons_to_choose, active='application', second_lessons=second_lessons,
                                   chosen=[int(request.form[k]) for k in request.form])
        else:
            if get_application() is not None:
                chosen_lessons = ChosenLesson.query.filter_by(application=get_application()).all()
                chosen_required = [chosen for chosen in chosen_lessons if chosen.lesson.group_id != 3]
                for item in chosen_required:
                    db.session.delete(item)
            for item in request.form:
                db.session.add(ChosenLesson(lesson_id=request.form[item],
                                            application=get_application(), application_id=get_application().id))
                application = get_application()
                application.step = 1
                db.session.add(application)
            db.session.commit()
        return redirect(url_for('student.form_optional'))


class StudentOptionalLessons(MethodView):
    decorators = [role_required(USER), login_required]

    def get(self):
        chosen = None
        if get_application() is not None:
            chosen_lessons = ChosenLesson.query.filter_by(application=get_application()).all()
            chosen = [chosen for chosen in chosen_lessons if chosen.lesson.group_id == 3]

        third_lessons = Lesson.query.filter_by(group_id=3).all()
        return render_template('student/second_step.html', active='application', lessons=third_lessons,
                               chosen=[int(c.lesson.id) for c in chosen])

    def post(self):
        third_lessons = Lesson.query.filter_by(group_id=3).all()
        lesson_count_first = 0
        lesson_count_second = 0
        chosen_lessons = ChosenLesson.query.filter_by(application=get_application()).all()
        for lesson in chosen_lessons:
            if lesson.lesson.group_id != 3:
                lesson_count_first += lesson.lesson.first_year
                lesson_count_second += lesson.lesson.second_year
        for item in request.form:
            lesson = Lesson.query.filter_by(id=request.form[item]).first()
            lesson_count_first += lesson.first_year
            lesson_count_second += lesson.second_year
        max_lesson = Settings.query.filter_by(name='max_lessons').first().value
        min_lesson = Settings.query.filter_by(name='min_lessons').first().value
        if lesson_count_first >= int(min_lesson) <= lesson_count_second:
            if int(max_lesson) >= lesson_count_second and int(max_lesson) >= lesson_count_second:
                for lesson in chosen_lessons:
                    if lesson.lesson.group_id == 3:
                        db.session.delete(lesson)
                for item in request.form:
                    db.session.add(ChosenLesson(application=get_application(), lesson_id=request.form[item]))
                application = get_application()
                application.step = 2
                db.session.add(application)
                db.session.commit()
                return redirect(url_for('student.application'))
            else:
                flash(u'Pasirinkta per daug pamokų', 'error')
        else:
            flash(u'Pasirinkta per mažai pamokų', 'error')
        return render_template('student/second_step.html', active='application', lessons=third_lessons,
                               chosen=[int(request.form[c]) for c in request.form])


class StudentApplicationAdministrate(MethodView):
    decorators = [role_required(USER), login_required]

    def get(self):
        application = get_application()
        application.step = 3
        db.session.add(application)
        db.session.commit()
        return redirect(url_for('student.application'))


student.add_url_rule('/', view_func=StudentIndexPage.as_view('index'))
student.add_url_rule('/profile', view_func=StudentProfilePage.as_view('profile'))

student.add_url_rule('/form/1', view_func=StudentFormFirstStep.as_view('first_step'))
student.add_url_rule('/form/2', view_func=StudentFormSecondStep.as_view('second_step'))
student.add_url_rule('/form/3', view_func=StudentFormThirdStep.as_view('third_step'))

student.add_url_rule('/generate/pdf', view_func=StudentGeneratePdf.as_view('gen_pdf'))
student.add_url_rule('/form/delete', view_func=StudentDeleteApplication.as_view('delete_application'))

student.add_url_rule('/lessons', view_func=StudentLessonView.as_view('lessons'))
student.add_url_rule('/contact', view_func=StudentContacts.as_view('contacts'))
student.add_url_rule('/change_pass', view_func=StudentChangePassword.as_view('new_password'))
student.add_url_rule('/first_login', view_func=StudentFirstLogin.as_view('first_login'))

#form
student.add_url_rule('/application', view_func=StudentApplicationForm.as_view('application'))
student.add_url_rule('/application/form/required', view_func=StudentRequiredLessons.as_view('form_required'))
student.add_url_rule('/application/form/optional', view_func=StudentOptionalLessons.as_view('form_optional'))
student.add_url_rule('/application/administrate', view_func=StudentApplicationAdministrate.as_view('administrate'))