class NoFormException(AttributeError):

   def __init__(self, *args, **kwargs):
        AttributeError.__init__(self, *args, **kwargs)


class LessonFormValidator(object):

    def __init__(self, form=None):
        self.form = form

    def init_form(self, form):
        self.form = form

    def validate(self):
        if self.form is None:
            return NoFormException()
        else:
            return self.__validate()

    def __validate(self):
        print self.form