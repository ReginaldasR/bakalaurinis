import datetime


class ValidationError(ValueError):
    """
    Raised when a validator fails to validate its input.
    """
    def __init__(self, message='', *args, **kwargs):
        ValueError.__init__(self, message, *args, **kwargs)

class ExpectedDateFormat(object):
    def __init__(self, format, message=None):
        if not message:
            message = u'Wrong date format'
        self.message = message
        self.format = format

    def __call__(self,form, field):
        try:
            datetime.datetime.strptime(field.data, self.format)
        except Exception:
            raise  ValidationError(message=self.message)

class DateCantBePast(object):

    def __init__(self, message=None):
        if not message:
            message = u'Date can\'t be past'
        self.message = message

    def __call__(self,form, field):
        now = datetime.datetime.now().date()
        try:
            input_date = datetime.datetime.strptime(field.data, "%Y-%m-%d").date()
            if now > input_date:
                raise ValidationError(message=self.message)
        except ValueError:
            raise ValidationError(message=self.message)



