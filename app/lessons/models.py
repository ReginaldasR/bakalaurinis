from ..extensions import db
from sqlalchemy import Column
from sqlalchemy.sql.expression import _Null


class Group(db.Model):
    id = Column(db.Integer, primary_key=True)
    name = Column(db.String(100), unique=True, nullable=False)

    def __repr__(self):
        return '<Group > %s' % self.name

class LessonType(db.Model):
    id = Column(db.Integer, primary_key=True)
    name = Column(db.String(100), unique=True, nullable=False)

    def __repr__(self):
        return '<LessonType > %s' % self.name

class Lesson(db.Model):
    id = Column(db.Integer, primary_key=True)
    group_id = Column(db.Integer, db.ForeignKey('group.id'))
    group = db.relationship('Group')
    lesson_type_id = Column(db.Integer, db.ForeignKey('lesson_type.id'), nullable=True)
    lesson_type = db.relationship('LessonType')
    name = Column(db.String(64), unique=False, nullable=False)
    credits = Column(db.Integer, unique=False, nullable=False)
    first_year = Column(db.Integer, unique=False, nullable=False)
    second_year = Column(db.Integer, unique=False, nullable=False)
    level = Column(db.Integer, unique=False, nullable=False)

    def __repr__(self):
        return '<Lesson > %s %s %s' % (self.name, self.lesson_type, self.group)


class Application(db.Model):
    id = Column(db.Integer, primary_key=True)
    user = db.relationship('User')
    user_id = Column(db.Integer, db.ForeignKey('user.id'), nullable=False, unique=True)
    step = Column(db.Integer, nullable=True, unique=False)
    seen = Column(db.Integer, nullable=True, unique=False)
    confirm = Column(db.Integer, nullable=True, unique=False)


class ChosenLesson(db.Model):
    id = Column(db.Integer, primary_key=True)
    application = db.relationship('Application')
    application_id = Column(db.Integer, db.ForeignKey('application.id'), nullable=False, unique=False)
    lesson = db.relationship('Lesson')
    lesson_id = Column(db.Integer, db.ForeignKey('lesson.id'), unique=False, nullable=False)