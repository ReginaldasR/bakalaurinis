# -*- coding: utf-8 -*-
import StringIO
import mimetypes
import random
import string
from subprocess import check_output
from app.auth.utils import hash_password
from app.extensions import db
from app.lessons.models import Group, Lesson, ChosenLesson, Application
from app.users.models import Person, User, USER
import config
from flask import flash, Response
import os
from sqlalchemy.sql.functions import user
from werkzeug.datastructures import Headers
from werkzeug.utils import secure_filename
import xlrd
from xlwt.Formatting import Font, Borders, Alignment
import xlwt
from xlwt.Style import XFStyle


def generate_username():
    username = random_char(8)
    user = User.query.filter_by(username=username).first()
    if user is None:
        return username
    else:
        return generate_username()


def create_users(file):
    filename = secure_filename(file.filename)
    file.save(os.path.join(config.UPLOAD_FOLDER, filename))
    workbook = xlrd.open_workbook(os.path.join(config.UPLOAD_FOLDER, filename))
    worksheet = workbook.sheet_by_name(workbook.sheet_names()[0])
    num_rows = worksheet.nrows - 1
    curr_row = 0
    while curr_row < num_rows:
        curr_row += 1
        row = worksheet.row(curr_row)
        print row
        class_info = list(row[2].value)
        try:
            int(class_info[-2])
        except ValueError:
            flash(u'Netinkamo formato duomenys', 'error')
            os.remove(os.path.join(config.UPLOAD_FOLDER, filename))
            return False
        check_person = Person.query.filter_by(first_name=row[1].value, second_name=row[0].value).first()
        if check_person is not None:
            os.remove(os.path.join(config.UPLOAD_FOLDER, filename))
            flash(u'Negalima dubliuoti duomenu', 'error')
            return False
        gender = 0 #default to man
        if row[3].value == 'mot.':
            gender = 1
        person = Person(first_name=row[1].value, second_name=row[0].value,
                        class_number=class_info[-2], class_letter=class_info[-1], gender=gender)
        db.session.add(person)
        unique_username = generate_username()
        unique_password = random_char(6)
        salt, password = hash_password(unique_password)
        user = User(username=unique_username, password=password, salt=salt, role=USER, person=person,
                    person_id=person.id, first_password=unique_password, first_login=0)
        db.session.add(user)
        db.session.commit()
    os.remove(os.path.join(config.UPLOAD_FOLDER, filename))
    return True

def random_char(y):
       return ''.join(random.choice(string.ascii_letters) for x in range(y))

def gen_xls(class_info):
    wbk = xlwt.Workbook()
    #sheet = wbk.add_sheet('sheet 1')
    # indexing is zero based, row then column
    #sheet.write(0,1,'test text')

    wbk = generate_sheet(wbk, "11 kl.", class_info, 1)
    wbk = generate_sheet(wbk, "12 kl.", class_info, 2)

    #########################
    # Code for creating Flask
    # response
    #########################
    response = Response()
    response.status_code = 200


    output = StringIO.StringIO()
    wbk.save(output)
    response.data = output.getvalue()

    ################################
    # Code for setting correct
    # headers for jquery.fileDownload
    #################################
    filename = class_info+'.xls'
    mimetype_tuple = mimetypes.guess_type(filename)

    #HTTP headers for forcing file download
    response_headers = Headers({
            'Pragma': "public",  # required,
            'Expires': '0',
            'Cache-Control': 'must-revalidate, post-check=0, pre-check=0',
            'Cache-Control': 'private',  # required for certain browsers,
            'Content-Type': mimetype_tuple[0],
            'Content-Disposition': 'attachment; filename=\"%s\";' % filename,
            'Content-Transfer-Encoding': 'binary',
            'Content-Length': len(response.data)
        })

    response.headers = response_headers
    return response


def parse_full_name(user):
    return user.person.first_name + " " + user.person.second_name


def parse_gender(user):
    if user.person.gender == 0:
        return "vyr."
    else:
        return "mot."


def generate_sheet(workbook, sheet_name, class_info, year):
    sheet = workbook.add_sheet(sheet_name, cell_overwrite_ok=True)
    users = User.query.all()
    cur_class_persons = Person.query.filter_by(class_number=int(class_info[:1]),
                                               class_letter=class_info[1:]).all()
    class_users = []
    for user in users:
        if user.person in cur_class_persons:
            class_users.append(user)
    start_x = 2
    start_y = 0
    count = 0

    font1 = Font()
    font1.bold = True

    border = Borders()
    border.right = Borders.HAIR
    border.left = Borders.HAIR
    border.top = Borders.HAIR
    border.bottom = Borders.HAIR

    style1 = XFStyle()
    style1.font = font1
    style1.borders = border

    alignment = xlwt.Alignment()
    alignment.horz = xlwt.Alignment.HORZ_CENTER
    alignment.vert = xlwt.Alignment.VERT_CENTER

    style1.alignment = alignment

    style_horizontal = XFStyle()
    align = Alignment()
    align.rota = 0x5a
    style_horizontal.alignment = align
    style_horizontal.borders = border
    style_horizontal.font = font1

    grouped_lessons = {}
    start_point_1 = 3
    start_point_2 = 0
    end_point = 3
    groups = Group.query.all()
    print groups
    for group in groups:
        grouped_lessons[group.name] = Lesson.query.filter_by(group=group).all()
        sheet.write_merge(0, 0, start_point_1 + start_point_2, end_point + len(grouped_lessons[group.name])-1, group.name, style1)
        start_point_1 += start_point_2
        start_point_2 = len(grouped_lessons[group.name])
        end_point += start_point_2

    start_lesson_names = 3
    for group in grouped_lessons:
        for lesson in grouped_lessons[group]:
            sheet.write(1,start_lesson_names, lesson.name, style_horizontal)
            start_lesson_names += 1
    sheet.write(1, start_y, "Nr.", style1)
    sheet.write(1, start_y + 1, "Vardas Pavarde.", style1)
    sheet.write(1, start_y + 2, "Lytis", style1)

    sheet.col(1).width = 6666
    sheet.row(1).height = 4000
    print len(Lesson.query.all())
    lessons = Lesson.query.all()
    for i in range(3,len(lessons)+3,1):
        sheet.col(i).width = 1200
    groups = Group.query.all()
    while count < len(class_users):
        sheet.write(start_x, start_y, count + 1, style1)
        fontNoBold = Font()
        fontNoBold.bold = False
        style1.font = fontNoBold
        sheet.write(start_x, start_y + 1, parse_full_name(class_users[count]), style1)
        sheet.write(start_x, start_y + 2, parse_gender(class_users[count]), style1)
        for_loop_start = 3
        application = Application.query.filter_by(user=class_users[count]).first()
        chosen_person_lessons = ChosenLesson.query.filter_by(application=application).all()
        start_owerite = 0
        for i in lessons:
            sheet.write(start_x, start_y + for_loop_start+start_owerite, "", style1)
            start_owerite += 1
        for lesson in chosen_person_lessons:
            inndex_of_lesson = lessons.index(lesson.lesson)
            if year == 1:
                credit = lesson.lesson.first_year
            elif year == 2:
                credit = lesson.lesson.second_year
            sheet.write(start_x, start_y + for_loop_start+inndex_of_lesson, credit, style1)
        count += 1
        start_x += 1
    return workbook