# -*- coding: utf-8 -*-
import hashlib
import mimetypes
import shutil
import datetime
from app.admin.utils import create_users, gen_xls
from app.emails import send_email
import config
from flask.ext.weasyprint import render_pdf, HTML
from flask.json import jsonify
from flask.wrappers import Response
import os
from app.admin.forms import SystemSettings, GroupForm, LessonTypeForm
from app.auth.autorization import role_required
from app.auth.utils import hash_password
from app.extensions import db
from app.lessons.models import Group, Lesson, LessonType, Application, ChosenLesson
from app.users.models import Person, ADMIN, User, USER
from app.validators.custom_from_validators import LessonFormValidator
from app.auth.authentication import login_required
from flask import Blueprint, render_template, request, flash, url_for, abort, redirect, send_from_directory, session
from ..settings import Settings
from sqlalchemy.sql.functions import min
from werkzeug.datastructures import Headers

admin = Blueprint('admin', __name__, url_prefix='/admin')

@admin.route('/' , methods=['GET',])
@login_required
def index():
    return render_template('admin/index.html', active='index')

@admin.route('/system-settings', methods=['GET', 'POST'])
def system_settings():
    form = SystemSettings()
    max_lessons = Settings.query.filter_by(name="max_lessons").first().value
    min_lessons = Settings.query.filter_by(name="min_lessons").first().value
    date_until = Settings.query.filter_by(name="date_until").first().value

    if request.method == 'POST' and form.validate():
        print form.min_lessons.data
        if int(form.max_lessons.data) < int(form.min_lessons.data):
            print max_lessons
            print min_lessons
            flash(u'Minimalus pamokų skaičius negali būti didesnis už maksimalų', 'error')
            return redirect(url_for('admin.system_settings'))
        for key, value in form.data.iteritems():
            setting = Settings.query.filter_by(name=key).first()
            if setting:
                setting.value = value
            else:
                db.session.add(Settings(name=key, value=value))
            db.session.commit()
        flash(u'Sėkmingai atnaujinti nustatymai', 'success')
        return redirect(url_for('admin.system_settings'))
    return render_template('admin/systemSettings.html', form=form,
                           max_lessons=max_lessons, min_lessons=min_lessons,
                           date_until=date_until, active='system')

@admin.route('/settings/groups', methods=['GET', 'POST'])
@login_required
@role_required(ADMIN)
def groups():
    form = GroupForm()
    if request.method == 'POST' and form.validate():
        db.session.add(Group(name=form.group_name.data))
        db.session.commit()
        flash(u'Sėkmingai sukurta nauja grupė', 'success')
    groups = Group.query.all()

    return  render_template('admin/goupForm.html', form=form, groups=groups, active='groups')

@admin.route('/settings/groups/delete/<int:group_id>', methods=['GET'])
@login_required
@role_required(ADMIN)
def delete_group(group_id):
    try:
        group = Group.query.filter_by(id=group_id).first()
        db.session.delete(group)
        db.session.commit()
        flash(u'Sėkmingai ištrinta grupė', 'success')
    except Exception:
        abort(404)
    return redirect(url_for('admin.groups'))

@admin.route('settings/groups/update/<int:group_id>', methods=['POST', ])
@login_required
@role_required(ADMIN)
def update_group(group_id):
    group = Group.query.filter_by(id=group_id).first()
    group.name = request.form['group_name']
    db.session.add(group)
    db.session.commit()
    return redirect(url_for('admin.groups'))

@admin.route('/settings/types', methods=['GET', 'POST'])
@login_required
@role_required(ADMIN)
def types():
    form = LessonTypeForm()
    if request.method == 'POST' and form.validate():
        db.session.add(LessonType(name=form.lesson_type_name.data))
        db.session.commit()
        flash(u'Sėkmingai sukurta naujas pamokų tipas', 'success')
    types = LessonType.query.all()

    return  render_template('admin/lesson_type.html', form=form, types=types, active='types')

@admin.route('/settings/lessons', methods=['GET', 'POST',])
@login_required
@role_required(ADMIN)
def get_all_lessons():
    if request.method == 'POST':
        form = request.form
        validator = LessonFormValidator(form=form)
        errors = validator.validate()
        if not errors:
            if int(form['second_year']) + int(form['first_year']) != int(form['credits']):
                flash(u'Netinkamai įvesti duomenys', 'error')
                return redirect(url_for(u'admin.get_all_lessons'))
            group = Group.query.filter_by(id=form['group']).first()
            lesson_type = LessonType.query.filter_by(id=form['lesson_type']).first()
            if lesson_type is not None:
                lesson = Lesson(group_id=group.id, group=group,lesson_type=lesson_type, lesson_type_id=lesson_type.id, name=form['name'], credits=form['credits'],
                    first_year=form['first_year'], second_year=form['second_year'], level=form['level'])
            else:
                lesson = Lesson(group_id=group.id, group=group, name=form['name'], credits=form['credits'],
                    first_year=form['first_year'], second_year=form['second_year'], level=form['level'])
            db.session.add(lesson)
            db.session.commit()
    lessons = Lesson.query.all()
    groups = Group.query.all()
    lesson_types = LessonType.query.all()
    return  render_template('admin/lessons.html',
                            lessons=lessons, groups=groups, lesson_types=lesson_types,
                            selected={'type':1,'group':1}, active='lessons')

@admin.route('/settings/lessons/<int:lesson_id>', methods=['GET', 'POST', ])
@login_required
@role_required(ADMIN)
def lesson_page(lesson_id):
    lesson = Lesson.query.filter_by(id=lesson_id).first()
    groups = Group.query.all()
    lesson_types = LessonType.query.all()
    if request.method == 'POST':
        form = request.form
        validator = LessonFormValidator(form=form)
        errors = validator.validate()
        if int(form['second_year']) + int(form['first_year']) != int(form['credits']):

            flash(u'Netinkamai įvesti duomenys', 'error')
            return redirect(url_for('admin.lesson_page', lesson_id=lesson_id))
        if not errors:
            lesson.name = form['name']
            lesson.group = Group.query.filter_by(id=form['group']).first()
            lesson.group_id = form['group']
            if form['lesson_type'] is not None:
                lesson.lesson_type = LessonType.query.filter_by(id=form['lesson_type']).first()
                lesson.lesson_type_id = form['lesson_type']
            lesson.credits = form['credits']
            lesson.first_year = form['first_year']
            lesson.second_year = form['second_year']
            lesson.level = form['level']
            db.session.commit()
            flash(u'Sėkmingai atjauninta pamoka', 'success')
            return (redirect(url_for('admin.get_all_lessons')))
    return render_template('admin/lesson.html',
                           active='lessons', lesson=lesson, groups=groups, lesson_types=lesson_types,
                           selected={'type':lesson.lesson_type_id,'group':lesson.group_id})

@admin.route('/settings/lessons/<int:lesson_id>/delete', methods=['GET', ])
@login_required
@role_required(ADMIN)
def delete_lesson(lesson_id):
    try:
        lesson = Lesson.query.filter_by(id=lesson_id).first()
        db.session.delete(lesson)
        db.session.commit()
        flash(u'Sėkmingai ištrinta pamoka', 'success')
    except Exception:
        abort(404)
    return redirect(url_for('admin.get_all_lessons'))


@admin.route('/settings/users/', methods=['GET', 'POST', ])
@login_required
@role_required(ADMIN)
def users():
    users = User.query.all()
    if request.method == 'POST':
        form = request.form
        person = Person(first_name=form['name'], second_name=form['last_name'],
                        email=form['email'], class_number=form['class_number'], class_letter=form['class_letter'])
        db.session.add(person)
        salt, password = hash_password(form['password'])
        user = User(username=form['username'], password=password, salt=salt, role=form['role'], person=person, person_id=person.id, first_login=1)
        db.session.add(user)
        db.session.commit()
        flash(u'Sėkmingai sukurtas sistemos vartotojas', 'success')
    return render_template('admin/users.html', users=users, active='users')


@admin.route('/settings/import-users/', methods=['POST', ])
@login_required
@role_required(ADMIN)
def import_users():
    file = request.files['file']
    file_extension = file.filename.split('.')[-1]
    if file_extension == 'xls':
        if create_users(file):
            return redirect(request.referrer)
        else:
            return redirect(request.referrer)
    else:
        flash(u'Pasirinkote netinkamo formato failą', 'error')
        return redirect(url_for('admin.users'))

@admin.route('/settings/user/<int:id>', methods=['GET', 'POST'])
@login_required
@role_required(ADMIN)
def user_page(id):
    user = User.query.filter_by(id=id).first()
    application = Application.query.filter_by(user=user).first()
    if request.method == 'POST':
        form = request.form
        person = user.person
        person.first_name = form['name']
        person.second_name = form['last_name']
        person.email = form['email']
        person.class_letter = form['class_letter']
        person.class_number = form['class_number']
        person.gender = form['gender']
        db.session.commit()
        flash(u'Sėkmingai atnaujinta vartotojo informacija','success')
    session['user_id_for_application'] = user.id
    return render_template('admin/user_page.html', active='users', user=user, application=application)


@admin.route('/settings/user/delete/<int:id>', methods=['GET'])
@login_required
@role_required(ADMIN)
def delete_user(id):
    user = User.query.filter_by(id=id).first()
    application = Application.query.filter_by(user=user).first()
    if application is not None:
        chosen_lessons = ChosenLesson.query.filter_by(application=application)
        if chosen_lessons is not None:
            for lesson in chosen_lessons:
                db.session.delete(lesson)
        db.session.delete(application)
    db.session.delete(user)
    db.session.delete(user.person)
    db.session.commit()
    flash(u'Sekmingai ištrintas vartotojas', 'success')
    return redirect(url_for('admin.users'))

@admin.route('/print/students', methods=['GET'])
@login_required
@role_required(ADMIN)
def print_students():
    students = User.query.filter_by(role=USER).all()
    html = render_template('admin/pdf/users.html', users=students)
    return render_pdf(HTML(string=html))

@admin.route('/classes', methods=['GET'])
@login_required
@role_required(ADMIN)
def classes():
    users = User.query.all()
    classes = set([str(str(user.person.class_number)+str(user.person.class_letter)) for user in users if user.role == USER])
    splited_classes = {}
    for current_class in classes:
        girls = len(Person.query.filter_by(class_number=int(current_class[:1]),
                                       class_letter=current_class[1:], gender=1).all())
        mans = len(Person.query.filter_by(class_number=int(current_class[:1]),
                                       class_letter=current_class[1:], gender=0).all())
        splited_classes[current_class] = {'students': girls + mans,
                                          'girls': girls,
                                          'mans': mans}
    return render_template('admin/classes.html', classes=splited_classes, active='classes')


@admin.route('/classes/<info>', methods=['GET'])
@login_required
@role_required(ADMIN)
def class_info(info):
    users = User.query.all()
    cur_class_persons = Person.query.filter_by(class_number=int(info[:1]),
                                       class_letter=info[1:]).all()
    persons = []
    for user in users:
        if user.person in cur_class_persons:
            persons.append({'user' :user, 'application' : Application.query.filter_by(user=user).first()} )
    return render_template('admin/class_info.html', users=persons, active='classes', info=info)

@admin.route('/statistic/<info>', methods=['GET', 'POST'])
@login_required
@role_required(ADMIN)
def get_report(info):
    return gen_xls(info)

@admin.route('/database', methods=['GET', 'POST'])
@login_required
@role_required(ADMIN)
def databases():
    if request.method == 'POST':
        file = request.files['file']
        file_extension = file.filename.split('.')[-1]
        if file_extension == 'db':
            os.renames(config.PATH_TO_DB, os.path.join(config.basedir, 'oldDb.db'))
            file.save(config.PATH_TO_DB)
        else:
            flash(u'Netinkamas failo formatas', 'error')
    return render_template('admin/database.html', active='database')

@admin.route('/database/backup', methods=['GET'])
@login_required
@role_required(ADMIN)
def database_backup():
    original_db = config.PATH_TO_DB
    backup_db = config.PATH_BACKUP_TO_DB
    succesfuly = False
    counter = 0
    while succesfuly is False:
        shutil.copy2(original_db, backup_db)
        real_db_checksum = hashlib.md5(open(original_db).read()).hexdigest()
        backup_db_checksum = hashlib.md5(open(backup_db).read()).hexdigest()
        if real_db_checksum == backup_db_checksum:
            succesfuly = True
        else:
            counter += 1
        if counter == 10:
            flash(u'Nepavyksta padaryti duomenu bazes kopijos', 'error')
            return redirect(databases())

    response = Response()
    response.status_code = 200

    response.data = open(backup_db).read()

    ################################
    # Code for setting correct
    # headers for jquery.fileDownload
    #################################
    filename = 'Backup_database.db'
    mimetype_tuple = mimetypes.guess_type(filename)

    #HTTP headers for forcing file download
    response_headers = Headers({
            'Pragma': "public",  # required,
            'Expires': '0',
            'Cache-Control': 'must-revalidate, post-check=0, pre-check=0',
            'Cache-Control': 'private',  # required for certain browsers,
            'Content-Type': mimetype_tuple[0],
            'Content-Disposition': 'attachment; filename=\"%s\";' % filename,
            'Content-Transfer-Encoding': 'binary',
            'Content-Length': len(response.data)
        })

    response.headers = response_headers
    return response

@admin.route('/notifications/count', methods=['GET', ])
@login_required
@role_required(ADMIN)
def notifications_count():
    applications = Application.query.filter_by(step=3, seen=0).all()
    return jsonify(count=len(applications))

@admin.route('/notifications', methods=['GET', ])
@login_required
@role_required(ADMIN)
def notifications():
    applications = Application.query.filter_by(step=3, seen=0).all()
    return render_template('admin/notifications.html', applications=applications)

@admin.route('/user/application/<int:user_id>', methods=['GET', ])
@login_required
@role_required(ADMIN)
def user_application(user_id):
    lesson_count_first = 0
    lesson_count_second = 0
    user = User.query.filter_by(id=user_id).first()
    application = Application.query.filter_by(user=user).first()
    if application:
        chosen_lessons = ChosenLesson.query.filter_by(application=application).all()
        for lesson in chosen_lessons:
            lesson_count_first += lesson.lesson.first_year
            lesson_count_second += lesson.lesson.second_year
        session['user_id_for_application'] = user_id
        application.seen = 1
        db.session.add(application)
        db.session.commit()
    return render_template('admin/user_application.html', application=application, first=lesson_count_first, second=lesson_count_second, student_id=user_id)

@admin.route('/statistic', methods=['GET', ])
@login_required
@role_required(ADMIN)
def statistic():
    lessons_statistic = {}
    all_students = User.query.filter_by(role=USER).all()
    all_lessons = Lesson.query.all()
    for lesson in all_lessons:
        lessons_statistic[lesson] = 0
        for user in all_students:
            application = Application.query.filter_by(user=user).first()
            users_chosen_lessons = ChosenLesson.query.filter_by(application=application).all()
            for user_lesson in users_chosen_lessons:
                if lesson == user_lesson.lesson:
                    lessons_statistic[lesson] += 1
    teachers = lessons_statistic
    return render_template('admin/statistic.html', active='statistic', lessons=lessons_statistic, student_count=len(all_students), teachers=teachers)

@admin.route('/delete/class/<class_info>', methods=['GET',])
@login_required
@role_required(ADMIN)
def delete_class(class_info):
    users = User.query.all()
    cur_class_persons = Person.query.filter_by(class_number=int(class_info[:1]),
                                       class_letter=class_info[1:]).all()
    for user in users:
        if user.person in cur_class_persons:
            application = Application.query.filter_by(user=user).first()
            if application is not None:
                chosen_lessons = ChosenLesson.query.filter_by(application=application).all()
                for lesson in chosen_lessons:
                    db.session.delete(lesson)
                db.session.delete(application)
            db.session.delete(user.person)
            db.session.delete(user)
            db.session.commit()
            flash(u'Sėkmingai ištrinta klasė ir visi jos mokiniai', 'success')
    return redirect(url_for('admin.classes'))

@admin.route('/gen_pdf/<int:student_id>', methods=['GET',])
@login_required
@role_required(ADMIN)
def gen_pdf(student_id):
    date_now = datetime.datetime.now()
    date = {'year': date_now.year}
    if len(str(date_now.month)) == 2:
        date['month'] = date_now.month
    else:
        date['month'] = '0' + str(date_now.month)
    if len(str(date_now.day)) == 2:
        date['day'] = date_now.day
    else:
        date['day'] = '0' + str(date_now.day)
    user = User.query.filter_by(id = student_id).first()
    first_year = int(date_now.year) + 1
    second_year = int(date_now.year) + 2
    chosen_lessons = ChosenLesson.query.filter_by(application=Application.query.filter_by(user_id=student_id).first()).all()
    lesson_to_template = {}
    lesson_groups = set([lesson.lesson.group for lesson in chosen_lessons])
    first_lesson_count = 1
    second_lesson_count = 1
    for group in lesson_groups:
        lesson_to_template[group.name] = [lesson.lesson for lesson in chosen_lessons if
                                          lesson.lesson.group == group]
    for lesson in chosen_lessons:
        first_lesson_count += lesson.lesson.first_year
        second_lesson_count += lesson.lesson.second_year
    max_lesson = Settings.query.filter_by(name='max_lessons').first()
    min_lesson = Settings.query.filter_by(name='min_lessons').first()

    html = render_template('student/pdf/test.html', user=user, date=date, first_year=first_year,
                           second_year=second_year, lessons=lesson_to_template, max=max_lesson, min=min_lesson,
                           second_lesson_count=second_lesson_count, first_lesson_count=first_lesson_count)
    return render_pdf(HTML(string=html))

@admin.route('/confirm/<int:student_id>', methods=['GET',])
@login_required
@role_required(ADMIN)
def confirm(student_id):
    user = User.query.filter_by(id=student_id).first()
    application = Application.query.filter_by(user=user).first()
    application.confirm = 1
    db.session.add(application)
    db.session.commit()
    text = "Jusu prasymas patvirtintas"
    send_email('Jusu prasymas patvirtintas',
                       config.ADMIN_EMAIL, [user.person.email, ], text, text)
    flash(u'Mokinys informuotas apie prašymo patvirtinimą', 'success')
    return redirect(url_for('admin.user_application', user_id=student_id))



@admin.route('/reject/<int:student_id>', methods=['POST',])
@login_required
@role_required(ADMIN)
def reject(student_id):
    user = User.query.filter_by(id=student_id).first()
    application = Application.query.filter_by(user_id=student_id).first()
    application.step = 2
    application.seen = 0
    application.confirm = 0
    db.session.add(application)
    db.session.commit()
    text = "Jusu prasymas atmestas \n " + request.form['reason']
    send_email('Jusu prasymas atmestas',
                       config.ADMIN_EMAIL, [user.person.email, ], text, text)
    flash(u'Mokinys informuotas apie prašymo atmetimą', 'success')
    return redirect(url_for('admin.user_application', user_id=student_id))