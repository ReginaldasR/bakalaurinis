from app.validators.form_validators import ExpectedDateFormat, DateCantBePast
from flask.ext.wtf import Form
from wtforms import TextField
from wtforms import validators


class SystemSettings(Form):
    max_lessons = TextField('max_lessons',
                            validators=[validators.Required(message="Maksimalus pamoku skaicius yra privalomas"),
                                        ])
    min_lessons = TextField('max_lessons',
                            validators=[validators.Required(message="Minimalus pamoku skaicius yra privalomas"),])
    date_until = TextField('date_until',
                           validators=[validators.Required(message="Data iki kurios priimamos paraiskos yra privalomas"),
                                       ExpectedDateFormat(format='%Y-%m-%d', message="Blogas datos formatas"),
                                       DateCantBePast("Data iki kurios galima priduoti paraiskas negali buti praeitis"),])

class GroupForm(Form):
    group_name = TextField('group_name', validators=[validators.Required(message="Dalyku grupes pavadinimas negali buti tuscias")])

class LessonTypeForm(Form):
    lesson_type_name = TextField('lesson_type_name')

