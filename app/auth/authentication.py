# -*- coding: utf-8 -*-
from functools import wraps
import logging
from app.auth.utils import check_password
from app.users.models import User, USER
from flask import flash, redirect, url_for, session, request, g
import jsonpickle


def login_required(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if 'logged' in session and 'subject' in session:
                if session['logged'] == True:
                    if g.user.first_login == 0 and g.user.role == USER:
                        return redirect(url_for('student.first_login'))
                    else:
                        return f(*args, **kwargs)
        else:
            logging.info("Not authored user")
            session['next'] = request.path
            flash(u'Jūs nesate prisijungias', 'error')
            return redirect('/login')
    return wrapper

def login_subject(user, password):
    if check_password(password, user.password, user.salt):
        subject = Subject(user.id, user.username, user.role)
        session['logged'] = True
        session['subject'] = jsonpickle.encode(subject)
        return True
    else:
        return False

def logout_subject():
    if 'logged' not in session and 'subject' not in session:
        flash(u'Jūs nesate prisijunges', 'error')
    else:
        session.pop('logged')
        session.pop('subject')
        flash(u'Sėkmingai atsijungėte nuo sistemos', 'success')

def user_mapper():
    subject = jsonpickle.decode(session['subject'])
    user = User.query.filter_by(id=subject.id, username=subject.username).first()
    return user

class Subject(object):

    def __init__(self, id, username, role):
        self.id = id
        self.username = username
        self.role = role

    def __repr__(self):
        return u'user id %s username %s and role %s' % (self.id, self.username, self.role)
