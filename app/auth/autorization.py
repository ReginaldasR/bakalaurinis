# -*- coding: utf-8 -*-
from functools import wraps
from flask import g, flash, redirect, request


def role_required(role, role2=None):
    def real_decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            if g.user is not None:
                if g.user.role == role:
                    return f(*args, **kwargs)
                elif role2 is not None:
                    if g.user.role == role2:
                        return f(*args, **kwargs)
                else:
                    flash(u'Neturite reikalingų teisių', 'error')
                    return redirect(request.referrer)
            else:
                return redirect('/login')
        return wrapper
    return real_decorator