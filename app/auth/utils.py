import hashlib
import uuid


def hash_password(password):
    salt = uuid.uuid4().hex
    hashed_password = hashlib.sha512(password + salt).hexdigest()
    return salt, hashed_password

def check_password(password, password_db, salt_db):
    password_check = hashlib.sha512(password + salt_db).hexdigest()
    return password_check == password_db