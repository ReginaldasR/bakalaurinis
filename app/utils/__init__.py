from StringIO import StringIO
from app.decorators import async
from xhtml2pdf import pisa

def create_pdf(pdf_data):
    pdf = StringIO()
    pisa.CreatePDF(StringIO(pdf_data), pdf)
    return pdf