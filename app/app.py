# -*- coding: utf-8 -*-
from datetime import timedelta
from .admin import admin
from .emails import send_email
from .student import student
from .auth import login_subject, logout_subject,user_mapper
from .users.models import User, ADMIN, USER
from .extensions import db, mail
from config import DEBUG, MAIL_SERVER, MAIL_USERNAME, MAIL_PASSWORD, MAIL_PORT, ALLOWED_EXTENSIONS
from flask import render_template, Flask, request, redirect, url_for, flash, g, session
from logging.handlers import RotatingFileHandler
import logging

DEFAULT_BLUEPRINTS = (
    admin,
    student,
)

app = Flask(__name__)
app.debug = DEBUG
app.permanent_session_lifetime = timedelta(minutes=100)

@app.route('/', methods=['GET', 'POST',])
def index():
    return render_template('index.html')

@app.route('/login', methods=['GET', 'POST',])
def login():
    if request.method == 'POST':
        form = request.form
        user = User.query.filter_by(username=form['username']).first()
        if user is not None and login_subject(user, form['password']):
            flash(u'Sėkmingai prisijungėte prie sistemos', 'success')
            if 'next' in session:
                next_url = session['next']
                session.pop('next')
                return redirect(request.url_root[:-1] + next_url)
            if user.role == USER:
                if user.first_login == 1:
                    return redirect(url_for('student.profile'))
                return redirect(url_for('student.index'))
            elif user.role == ADMIN:
                return redirect(url_for('admin.index'))
        else:
            flash(u'Klaidingi prisijungimo duomenys', 'error')
    return render_template('index.html')

@app.route('/logout', methods=['GET'])
def logout():
    logout_subject()
    return redirect(url_for('login'))

@app.before_request
def before_request():
    if 'subject' in session:
        g.user = user_mapper()
    else:
        g.user = None

def configure_blueprints(app, blueprints=None):
    """Configure blueprints in views."""
    
    if blueprints is None:
        blueprints = DEFAULT_BLUEPRINTS

    for blueprint in blueprints:
        app.register_blueprint(blueprint)

def configure_error_handlers(app):
    
    @app.errorhandler(404)
    def page_not_found(e):
        return render_template('errors/404.html'), 404

    @app.errorhandler(500)
    def internal_server_error(e):
        return "Server Error", 500


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

handler = RotatingFileHandler('foo.log', maxBytes=10000, backupCount=1)
handler.setLevel(logging.INFO)
app.logger.addHandler(handler)

if not app.debug and MAIL_SERVER != '':

    from logging.handlers import SMTPHandler

    credentials = None
    if MAIL_USERNAME or MAIL_PASSWORD:
        credentials = (MAIL_USERNAME, MAIL_PASSWORD)
    mail_handler = SMTPHandler((MAIL_SERVER, MAIL_PORT), 'no-reply@' + MAIL_SERVER, ['reginaldasr@gmail.com'], 'Panevezio Vytauto Zemkalnio gimnacija', credentials)
    mail_handler.setLevel(logging.ERROR)
    app.logger.addHandler(mail_handler)


app.config.from_object('config')
configure_error_handlers(app)
db.init_app(app)
mail.init_app(app)
configure_blueprints(app)
from .settings import models
from .lessons import models
from .users import models


