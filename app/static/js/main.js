$(document).ready(function(){

    $("#lesson-cancel").click(function(){
        $("#lesson-new").show();
        $("#import-users").show();
        $("#form-content").slideToggle("fast");
    });

    $('#lesson-new').click(function(){
        $("#form-content").slideToggle("fast");
        $("#lesson-new").hide();
        $("#import-users").hide();
    });

    $( "#date_until" ).datepicker({ closeText: 'Uždaryti',
            prevText: '&#x3c;Atgal',
            nextText: 'Pirmyn&#x3e;',
            currentText: 'Šiandien',
            monthNames: ['Sausis','Vasaris','Kovas','Balandis','Gegužė','Birželis',
            'Liepa','Rugpjūtis','Rugsėjis','Spalis','Lapkritis','Gruodis'],
            monthNamesShort: ['Sau','Vas','Kov','Bal','Geg','Bir',
            'Lie','Rugp','Rugs','Spa','Lap','Gru'],
            dayNames: ['sekmadienis','pirmadienis','antradienis','trečiadienis','ketvirtadienis','penktadienis','šeštadienis'],
            dayNamesShort: ['sek','pir','ant','tre','ket','pen','šeš'],
            dayNamesMin: ['Se','Pr','An','Tr','Ke','Pe','Še'],
            weekHeader: 'Wk',
            dateFormat: 'yy-mm-dd',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: '' });

    $("#role").on('change', function(){
        var role = this.value;
        if (role == 0) {
            $("#student-info").slideToggle();
        } else {
            $("#student-info").slideToggle();
        }
    });

    $('#upload-fake').on("click" , function (event) {
        event.preventDefault();
        $('#upload-real').click();
    });

    $("#upload-real").change(function(){
        var filename = this.value.split(/(\\|\/)/g).pop();
        $("#file-name").text(filename);
    });

    $("#import-users").click(function(){
        $("#lesson-new").hide();
        $("#import-users").hide();
        $("#import-students").slideToggle();
    });

    $("#cancel-import").click(function(event){
        event.preventDefault();
        $("#lesson-new").show();
        $("#import-users").show();
        $("#import-students").slideToggle();
    });
});