from ..extensions import db
from sqlalchemy import Column


class Settings(db.Model):
    id = Column(db.Integer, primary_key=True)
    name = Column(db.String(64), index=True, unique=True)
    value = Column(db.String(64), index=True, unique=True)

    def __repr__(self):
        return '<Setting > %s' % self.name
