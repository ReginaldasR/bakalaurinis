from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
address = Table('address', pre_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('email', String),
    Column('person_id', Integer),
)

person = Table('person', pre_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('name', String),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['address'].drop()
    pre_meta.tables['person'].drop()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['address'].create()
    pre_meta.tables['person'].create()
