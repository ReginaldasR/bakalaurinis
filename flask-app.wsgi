#!/usr/bin/python
import os
current_dir = os.getcwd()

activate_this = current_dir + '/flask/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))
import sys
import logging
logging.basicConfig(stream=sys.stderr)
sys.path.insert(0,"/var/www/Flask-App/")

from app import app as application

