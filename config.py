# -*- coding: utf8 -*-
import os
basedir = os.path.abspath(os.path.dirname(__file__))

#Application Debug mode
DEBUG = True

#WTForms config
CSRF_ENABLED = True
SECRET_KEY = 'aa9sd569a5695as9f5a9sd9ad69ad6as9d6as9'


# slow database query threshold (in seconds)
DATABASE_QUERY_TIMEOUT = 0.5

#SQLaclhemy configuration
PATH_TO_DB = os.path.join(basedir, 'app.db')
PATH_BACKUP_TO_DB = os.path.join(basedir, 'backup.db')
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + PATH_TO_DB
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')

#File upload
UPLOAD_FOLDER = os.path.join(basedir, 'app','static','uploads')
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}

# email server
MAIL_SERVER = 'smtp.googlemail.com'
MAIL_PORT = 465
MAIL_USE_TLS = False
MAIL_USE_SSL = True
MAIL_USERNAME = 'reginaldasr'
MAIL_PASSWORD = 'mundlu123mundlu'
ADMIN_EMAIL = 'reginaldasr@gmail.com'
